# Literature for the Policing In a Digital Society poster

1. M. Motoyama, D. McCoy, K. Levchenko, S. Savage, and G. M. Voelker, ‘An analysis of underground forums’, in Proceedings of the 2011 ACM SIGCOMM conference on Internet measurement conference, Berlin Germany: ACM, Nov. 2011, pp. 71–80. doi: 10.1145/2068816.2068824. (https://dl.acm.org/doi/10.1145/2068816.2068824)
2. C. Huang, Y. Guo, W. Guo, and Y. Li, ‘HackerRank: Identifying key hackers in underground forums’, International Journal of Distributed Sensor Networks, vol. 17, no. 5, p. 155014772110151, 2021, doi: 10.1177/15501477211015145. (https://journals.sagepub.com/doi/10.1177/15501477211015145)
3. S. Afroz, A. C. Islam, A. Stolerman, R. Greenstadt, and D. McCoy, ‘Doppelgänger Finder: Taking Stylometry to the Underground’, in 2014 IEEE Symposium on Security and Privacy, May 2014, pp. 212–226. doi: 10.1109/SP.2014.21. (https://dl.acm.org/doi/10.1109/SP.2014.21)
4.  Y. Fang, Y. Guo, C. Huang, and L. Liu, ‘Analyzing and Identifying Data Breaches in Underground Forums’, IEEE Access, vol. 7, pp. 48770–48777, 2019, doi: 10.1109/ACCESS.2019.2910229. (https://ieeexplore.ieee.org/document/8686093)
5. https://nl.wikipedia.org/wiki/Toeslagenaffaire
6. https://www.technologyreview.com/2016/11/22/107128/neural-network-learns-to-identify-criminals-by-their-faces/ 
7. https://www.insidehighered.com/admissions/article/2020/12/14/u-texas-will-stop-using-controversial-algorithm-evaluate-phd
8. The Menlo Report (2012), https://www.dhs.gov/sites/default/files/publications/CSD-MenloPrinciplesCORE-20120803_1.pdf

For more question or comments you can reach me at r.e.hoheisel@utwente.nl